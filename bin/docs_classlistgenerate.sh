#!/usr/bin/env bash
repo_dir=$(git rev-parse --show-toplevel)
. "bin/functions"
version=$(jq<"${repo_dir}"/metadata.json -r '.version')
name=$(jq<"${repo_dir}"/metadata.json -r '.name')
source_dir=${1:-'manifests/'}
target_file=${2:-'unified-policy-document/classes.md'}
[ -f $target_file ] && rm -f $target_file

echo >> $target_file
echo "## List of classes" >> $target_file
echo >> $target_file
echo "This is an automatically generated list of ${name} v${version} classes" >> $target_file
echo >> $target_file
for f in $(find $source_dir -name *.pp | grep manifests | grep -v fixtures); do
  a=$(grep ^class ${f} | sed 's/(//g' | sed 's/{//g' | sed 's/)//g' | sed 's/ *$//g' | sed 's/^class //g')
  b=$(grep @summary ${f} | sed 's/^# *@summary/ -/g')
  echo "  - **${a}**${b}" >> $target_file
done
echo >> $target_file