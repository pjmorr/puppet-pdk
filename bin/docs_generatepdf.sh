#!/usr/bin/env bash
# Generate PDF documentation
repo_dir=$(git rev-parse --show-toplevel)
. "/bin/functions"
cmits_example="/home/gitlab-runner/pimp/cmits-example"
version=$(jq<"${repo_dir}"/package.json -r '.version')
name=$(jq<"${repo_dir}"/package.json -r '.name')
module_file=${3:-'unified-policy-document/module.md'}

if [[ ! -d unified-policy-document ]]; then
  cp -a $cmits_example/unified-policy-document unified-policy-document
  cp -a $cmits_example/unclass unclass
  cp -a $cmits_example/build-products build-products
fi
cp  /home/gitlab-runner/pimp/make_config.ini unified-policy-document/make_config.ini
cp logo.png unified-policy-document/org_logo.png
#cp README.md unified-policy-document/README.md
if [[ -f /public/${name}-${version}.pdf ]]; then
  rm /public/${name}-${version}.pdf
fi
mkdir public ||  true

for m in $(ls *.md); do
  #echo $m
  cp ${m} unified-policy-document/${m}
  docker run -v $(pwd):/puppet -i jagregory/pandoc \
  -f markdown /puppet/unified-policy-document/${m} \
  -o /puppet/unified-policy-document/${m}.tex
done

#for f in $(cat module-toc.txt); do cat $f >> module.md ; done
#pandoc unified-policy-document/module.md -o unified-policy-document/module.tex

cd unified-policy-document || true

make > /dev/null 2>&1
mv main.pdf ${repo_dir}/public/${name}-${version}.pdf
make clean > /dev/null 2>&1