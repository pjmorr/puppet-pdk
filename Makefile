IMAGE = 'registry.ubermonitoring.com:4567/phil/puppet-pdk'
VERSION = '0.0.5'

NAME = $(IMAGE)
TAG = $(NAME):$(VERSION)
LATEST = $(NAME):latest
.PHONY: build push deploy
.DEFAULT: build

build:
	docker build -t $(TAG) .
	docker build -t $(LATEST) .

push: build
	docker push $(TAG)
	docker push $(LATEST)

deploy: push
	./install.sh

release: push
	git tag $(VERSION)
	git push origin $(VERSION)
